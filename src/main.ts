import { createApp } from 'vue';
import 'normalize.css/normalize.css';
import '@wikimedia/codex/dist/codex.style.css';
import './assets/style.less';
import App from './App.vue';
import { router } from './router';

createApp( App )
	.use( router )
	.mount( '#app' );
