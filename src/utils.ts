import { Backlink } from './types';

export function sortByLastTouched( a: Backlink, b: Backlink ) : number {
	return b.last_touched - a.last_touched;
}

export function sortByTitleAndNamespace( a: Backlink, b: Backlink ): number {
	if ( a.namespace === b.namespace ) {
		return a.title.toLowerCase().localeCompare( b.title.toLowerCase() );
	} else {
		return a.namespace < b.namespace ? -1 : 1;
	}
}
