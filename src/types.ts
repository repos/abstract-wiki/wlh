export type Namespace = {
	case: string,
	content: boolean,
	id: number,
	name: string,
	nonincludable: boolean,
	subpages: boolean
}

// This should correspond to the Article struct
// defined in main.rs in the API repo.
export type Backlink = {
	title: string,
	namespace: number,
	bytes: number,
	last_touched: number
}
