import { createRouter, createWebHistory } from 'vue-router';
import StartView from './views/StartView.vue';
import ResultsView from './views/ResultsView.vue';

export const router = createRouter( {
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			component: StartView,
			name: 'Start'
		},
		{
			path: '/pages/:pageId?',
			component: ResultsView,
			name: 'Results',
			props: ( route ) => {
				return {
					pageId: route.params.pageId,
					ns: route.query.ns,
					namespaceFilter: route.query.namespace_filter
				};
			}
		}
	]
} );
