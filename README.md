# WLH: a sortable What Links Here page

Corrosponding API: https://gitlab.wikimedia.org/repos/abstract-wiki/wlh-api

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support For `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Deployment

A `deploy_to_toolforge.sh` script lives in the `scripts/` directory. The script
assumes that you have been set up with SSH access on Toolforge and are a
maintainer of the WLH project.  You can run it like this:

```bash
# "myusername" should be replaced with your toolforge user name
npm run deploy -- myusername
```

### Deployment troubleshooting

If the script doesn't work (due to a file permission error on Toolforge, for
example), you may need to perform the steps manually.

First: Clear out the old `dist` folder on the remote host:

```bash
# SSH into toolforge
ssh myusername@login.toolforge.org

# On the remote machine, run the "become" command.
# This moves you to the project directory (now your home dir)
become wlh

# take ownership of the "dist" folder and delete it if necessary
take dist
rm -rf dist
```

Next: on your local machine, run the build command and then copy the resulting files over to the remote via SCP:

```bash
# Build the files; you may need to run npm install if you have not already done so
npm run build

# Copy the dist folder over to the remote host
scp -r dist myusername@login.toolforge.org:/data/project/wlh/dist
```

Next: SSH into the remote and `become` the WLH tool, and then replace the old
files with new ones after making a backup.

```bash
ssh myusername@login.toolforge.org
become wlh

# Remove the old backup
rm -rf public_html.bak

# Back up the current version
mv public_html public_html.bak

# Move the new files over to public_html to serve them
mv dist public_html

# Stop and re-start the webserver
webservice stop && webservice start
```
